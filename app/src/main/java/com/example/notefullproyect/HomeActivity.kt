package com.example.notefullproyect

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import com.example.notefullproyect.databinding.ActivityHomeBinding
import com.example.notefullproyect.time.CalendarFragment
import com.example.notefullproyect.task.presentation.RVTaskFragment
import com.example.notefullproyect.user.presentation.UserSettingsFragment

/***
 * Contains a fragmentContenedor and implements the menu of  the application.
 * @author Jessica Castillo
 */
class HomeActivity : AppCompatActivity() {

    private lateinit var binding: ActivityHomeBinding
    private lateinit var toogle: ActionBarDrawerToggle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val drawerLay = binding.drawerLayout
        val navView = binding.navView

        toogle = ActionBarDrawerToggle(this, drawerLay, R.string.open_drawer, R.string.close_drawer)
        drawerLay.addDrawerListener(toogle)
        toogle.syncState()

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        navView.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.nav_home -> {
                    val preferencesTask = this.applicationContext.getSharedPreferences(
                        "MySharedPreferences",
                        AppCompatActivity.MODE_PRIVATE
                    ).edit()
                    preferencesTask.putString("currentDate", "")
                    preferencesTask.apply()

                    supportFragmentManager.beginTransaction().apply {
                        replace(R.id.fragmentContainerView, RVTaskFragment())
                        commit()
                    }
                }
                R.id.nav_calendar -> {
                    supportFragmentManager.beginTransaction().apply {
                        replace(R.id.fragmentContainerView, CalendarFragment()).addToBackStack(
                            RVTaskFragment::class.java.name
                        )
                        commit()
                    }
                }
                R.id.nav_user -> {
                    supportFragmentManager.beginTransaction().apply {
                        replace(R.id.fragmentContainerView, UserSettingsFragment())
                        commit()
                    }
                }
            }
            drawerLay.closeDrawer(GravityCompat.START)
            true
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if (toogle.onOptionsItemSelected(item)) {
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}