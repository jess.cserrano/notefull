package com.example.notefullproyect

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import com.example.notefullproyect.database.NoteFullDb
import com.example.notefullproyect.databinding.ActivityMainBinding
import com.example.notefullproyect.priority.model.Priority
import com.example.notefullproyect.progress.model.Progress
import com.example.notefullproyect.user.presentation.ForgottenPasswordActivity
import com.example.notefullproyect.user.presentation.SignInActivity
import kotlinx.coroutines.launch

/**
 *It is the launch activity, reset the values of MySharedPreferences and sets Priority and Progress values.
 * Implements the application login.
 * @author Jessica Castillo
 */
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var room: NoteFullDb

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        room = NoteFullDb.getDatabase(applicationContext)

        /** resets all pre saved data */
        resetValues()
        /** preload the data if it doesn't exist */
        setPriorityValues()
        setProgressValues()

        binding.btnAccess.setOnClickListener {
            login()
        }

        binding.tvPulseHere.setOnClickListener {
            startActivity(Intent(this, SignInActivity::class.java))
        }

        binding.tvForgotPassword.setOnClickListener {
            startActivity(Intent(this, ForgottenPasswordActivity::class.java))
        }
    }

    private fun resetValues() {
        val preferencesTask = this.applicationContext.getSharedPreferences(
            "MySharedPreferences",
            MODE_PRIVATE
        ).edit()

        preferencesTask.putInt("userId", 0)
        preferencesTask.putString("currentDate", "")
        preferencesTask.putInt("taskId", 0)
        preferencesTask.putInt("subtaskId", 0)

        preferencesTask.apply()
    }

    /**
     *Implement the application login.
     * @author Jessica Castillo
     */
    private fun login() {
        val accessEmail = binding.etAccessEmail
        val accessPassword = binding.etAccessPassword

        if (accessEmail.text.toString().isNotEmpty() && accessPassword.text.toString()
                .isNotEmpty()
        ) {

            val user = room.userDao().getUserByEmail(accessEmail.text.toString())

            if (user != null) {
                if (user.password == accessPassword.text.toString()) {

                    val preferencesEditor =
                        applicationContext.getSharedPreferences("MySharedPreferences", MODE_PRIVATE)
                            .edit()
                    preferencesEditor.putInt("userId", user.id)
                    preferencesEditor.apply()

                    Toast.makeText(
                        applicationContext,
                        "Login correcto, bienvenido, ${user.name} ",
                        Toast.LENGTH_LONG
                    ).show()

                    startActivity(Intent(applicationContext, HomeActivity::class.java))
                } else {
                    Toast.makeText(
                        applicationContext,
                        "Contraseña incorrecta",
                        Toast.LENGTH_LONG
                    ).show()
                }
            } else {
                Toast.makeText(
                    applicationContext,
                    "El usuario no existe",
                    Toast.LENGTH_LONG
                ).show()
            }

        } else {
            Toast.makeText(
                applicationContext,
                "Uno o ambos campos no pueden estar vacíos.",
                Toast.LENGTH_LONG
            ).show()
        }

    }

    private fun setProgressValues() {
        val progressToDo = Progress(1, "Por hacer")
        val progressDoing = Progress(2, "Cursando")
        val progressDone = Progress(3, "Terminada")

        val listProgressValues = ArrayList<Progress>()
        listProgressValues.add(progressToDo)
        listProgressValues.add(progressDoing)
        listProgressValues.add(progressDone)

        val listProgress = room.progressDao().getProgressValues()
        if (listProgress.isEmpty()) {
            room.progressDao().insertProgressValues(listProgressValues)
        }
    }

    private fun setPriorityValues() {
        val priorityHigh = Priority(1, "Alta")
        val priorityMedium = Priority(2, "Media")
        val priorityLow = Priority(3, "Baja")

        val listPriorityValues = ArrayList<Priority>()
        listPriorityValues.add(priorityHigh)
        listPriorityValues.add(priorityMedium)
        listPriorityValues.add(priorityLow)

        val listPriority = room.priorityDao().getPriorityValues()
        if (listPriority.isEmpty()) {
            room.priorityDao().insertPriorityValues(listPriorityValues)
        }
    }
}
