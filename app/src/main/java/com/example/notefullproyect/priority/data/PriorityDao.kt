package com.example.notefullproyect.priority.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.notefullproyect.priority.model.Priority

/**
 * Functions that allow communication with the database for Priority entity.
 * @author Jessica Castillo
 */
@Dao
interface PriorityDao {

    @Insert
    fun insertPriorityValues(priorities: List<Priority>)

    @Query("SELECT * FROM Priority")
    fun getPriorityValues(): List<Priority>
}
