package com.example.notefullproyect.subtask.presentation

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.notefullproyect.R
import com.example.notefullproyect.database.NoteFullDb
import com.example.notefullproyect.databinding.ItemSubtaskBinding
import com.example.notefullproyect.subtask.model.SubTask
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

/**
 * From the item of the cardView adapt the content of the task to each cardView of the recyclerView.
 * @author Jessica Castillo
 */
class SubtaskAdapter(private val listSubtask: List<SubTask>) :
    RecyclerView.Adapter<SubtaskAdapter.ViewHolder>() {

    private lateinit var mListener: OnItemClickListener
    private lateinit var room: NoteFullDb

    /**
     * Interface that extends of OnClickListener
     * @author Jessica Castillo
     */
    interface OnItemClickListener : View.OnClickListener {

        /**
         * Method without implementer
         * @param position
         * @author Jessica Castillo
         */
        fun onItemClick(position: Int)
    }

    /**
     *set a value for my local variable mListener.
     * @param listener
     * @author Jessica Castillo
     */
    fun setOnItemClickListener(listener: OnItemClickListener) {
        mListener = listener
    }


    class ViewHolder(
        private val binding: ItemSubtaskBinding,
        listener: OnItemClickListener,
        room: NoteFullDb
    ) :
        RecyclerView.ViewHolder(binding.root) {

        var name = binding.tvNameCardSubTask
        var ivToDo = binding.ivToDo
        var ivDoing = binding.ivDoing
        var ivDone = binding.ivDone
        val roomDb = room


        fun bind(item: SubTask) {

            name.text = item.title
            binding.etMultiSubTask.text = item.description
            binding.tvSetPomodoroCardSub.text = item.quantityTime.toString()
            val priority = binding.tvSubTaskPriorityStatus

            when (item.priorityId) {
                1 -> priority.text = "Alta"
                2 -> priority.text = "Media"
                3 -> priority.text = "Baja"
            }

            when (item.progressId) {
                1 -> {
                    ivToDo.setImageResource(R.drawable.ic_circle_todo_check)
                    ivDoing.setImageResource(R.drawable.ic_circle_doing)
                    ivDone.setImageResource(R.drawable.ic_circle_done)
                    binding.cardViewSubTask.setCardBackgroundColor(Color.parseColor("#26EE9F93"))
                }
                2 -> {
                    ivDoing.setImageResource(R.drawable.ic_circle_doing_check)
                    ivToDo.setImageResource(R.drawable.ic_circle_todo)
                    ivDone.setImageResource(R.drawable.ic_circle_done)
                    binding.cardViewSubTask.setCardBackgroundColor(Color.parseColor("#26253DB5"))
                }
                3 -> {
                    ivDone.setImageResource(R.drawable.ic_circle_done_check)
                    ivToDo.setImageResource(R.drawable.ic_circle_todo)
                    ivDoing.setImageResource(R.drawable.ic_circle_doing)
                    binding.cardViewSubTask.setCardBackgroundColor(Color.parseColor("#66ACCCD3"))
                }
            }

            ivToDo.setOnClickListener {

                ivToDo.setImageResource(R.drawable.ic_circle_todo_check)
                ivDoing.setImageResource(R.drawable.ic_circle_doing)
                ivDone.setImageResource(R.drawable.ic_circle_done)
                binding.cardViewSubTask.setCardBackgroundColor(Color.parseColor("#26EE9F93"))
                updateProgressSubTask(roomDb, item.id, 1)
            }

            ivDoing.setOnClickListener {
                ivDoing.setImageResource(R.drawable.ic_circle_doing_check)
                ivToDo.setImageResource(R.drawable.ic_circle_todo)
                ivDone.setImageResource(R.drawable.ic_circle_done)
                binding.cardViewSubTask.setCardBackgroundColor(Color.parseColor("#26253DB5"))
                updateProgressSubTask(roomDb, item.id, 2)
            }

            ivDone.setOnClickListener {
                ivDone.setImageResource(R.drawable.ic_circle_done_check)
                ivToDo.setImageResource(R.drawable.ic_circle_todo)
                ivDoing.setImageResource(R.drawable.ic_circle_doing)
                binding.cardViewSubTask.setCardBackgroundColor(Color.parseColor("#66ACCCD3"))
                updateProgressSubTask(roomDb, item.id, 3)

            }


        }

        init {
            itemView.setOnClickListener {

                listener.onItemClick(absoluteAdapterPosition)
            }
        }

        private fun updateProgressSubTask(room: NoteFullDb, subtaskId: Int, progressId: Int) {
            val subtask = room.subTaskDao().getSubtaskById(subtaskId)
            subtask.progressId = progressId

            GlobalScope.launch {
                room.subTaskDao().updateSubTask(subtask)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)

        room = NoteFullDb.getDatabase(parent.context)

        val listItemSubBinding = ItemSubtaskBinding.inflate(inflater, parent, false)
        return ViewHolder(listItemSubBinding, mListener, room)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.bind(listSubtask[position])
    }

    override fun getItemCount(): Int {
        return listSubtask.size
    }

}