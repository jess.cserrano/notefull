package com.example.notefullproyect.subtask.data

import androidx.room.*
import com.example.notefullproyect.subtask.model.SubTask

/**
 * Functions that allow communication with the database for SubTask entity.
 * @author Jessica Castillo
 */
@Dao
interface SubTaskDao {

    @Insert
    suspend fun insertSubTask(subTask: SubTask)

    @Query("SELECT * FROM SubTask WHERE taskId = :taskId ORDER BY priorityId ASC")
    fun getSubtasksByTaskId(taskId: Int): List<SubTask>

    @Query("SELECT * FROM SubTask WHERE taskId = :taskId and id = :id")
    fun getSubtasksByTaskIdAndSubtaskId(taskId: Int, id: Int): SubTask

    @Query("SELECT * FROM SubTask WHERE id = :id ")
    fun getSubtaskById(id: Int): SubTask

    @Update
    suspend fun updateSubTask(subtask: SubTask)

    @Delete
    suspend fun deleteSubTask(subtask: SubTask)

    @Query("DELETE FROM Subtask WHERE taskId = :taskId")
    suspend fun deleteSubTasksByTaskId(taskId: Int)
}