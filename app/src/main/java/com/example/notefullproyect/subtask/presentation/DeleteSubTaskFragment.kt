package com.example.notefullproyect.subtask.presentation

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.example.notefullproyect.R
import com.example.notefullproyect.database.NoteFullDb
import com.example.notefullproyect.databinding.FragmentDeleteSubTaskBinding
import com.example.notefullproyect.subtask.model.SubTask
import kotlinx.coroutines.launch

/**
 * Implements the functionality to delete a subtask in the database.
 * @author Jessica Castillo
 */
class DeleteSubTaskFragment : Fragment() {

    private var _binding: FragmentDeleteSubTaskBinding? = null
    private val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDeleteSubTaskBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val room = NoteFullDb.getDatabase(view.context)

        val preferencesEditor = view.context.applicationContext.getSharedPreferences(
            "MySharedPreferences",
            AppCompatActivity.MODE_PRIVATE
        )
        val subtaskId = preferencesEditor.getInt("subtaskId", 0)

        val subtask = getSubtaskById(room,subtaskId)

        deleteSubtask(room,subtask)
    }

    /**
     * Delete a SubTask in the database.
     * @param room
     * @param subtask
     * @author Jessica Castillo
     */
    private fun deleteSubtask(room: NoteFullDb, subtask: SubTask) {

        lifecycleScope.launch {
            room.subTaskDao().deleteSubTask(subtask)
        }

        Toast.makeText(context,"Subtarea eliminada", Toast.LENGTH_LONG).show()

        replaceFragment(SubTaskFragment())

    }

    private fun getSubtaskById(room: NoteFullDb, subtaskId: Int): SubTask {
       return room.subTaskDao().getSubtaskById(subtaskId)
    }

    private fun replaceFragment(fragment: Fragment) {
        requireActivity().supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentContainerView, fragment)
            commit()
        }
    }

}