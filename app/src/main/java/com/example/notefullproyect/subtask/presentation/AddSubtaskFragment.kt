package com.example.notefullproyect.subtask.presentation

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.example.notefullproyect.R
import com.example.notefullproyect.database.NoteFullDb
import com.example.notefullproyect.databinding.FragmentAddSubtaskBinding
import com.example.notefullproyect.subtask.model.SubTask
import kotlinx.coroutines.launch

/**
 * Implements the functionality to add a subtask in the database.
 * @author Jessica Castillo
 */
class AddSubtaskFragment : Fragment() {

    private var _binding: FragmentAddSubtaskBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAddSubtaskBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val room = NoteFullDb.getDatabase(view.context)

        val preferencesEditor = view.context.applicationContext.getSharedPreferences(
            "MySharedPreferences",
            AppCompatActivity.MODE_PRIVATE
        )
        val taskId = preferencesEditor.getInt("taskId", 0)

        val spnSubTask = binding.spnSubTask

        binding.btnAddSubTask.setOnClickListener {

            if (binding.etSubTaskName.text.isBlank()) {
                Toast.makeText(
                    context,
                    "Se debe introducir un valor para el campo nombre",
                    Toast.LENGTH_LONG
                ).show()
            } else {
                insertSubTask(room, spnSubTask, taskId)
                replaceFragment(SubTaskFragment())
            }

        }
        prioritySpinner(view, spnSubTask)

        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    replaceFragment(SubTaskFragment())
                }
            }
        )
    }

    private fun prioritySpinner(view: View, spinner: Spinner) {
        ArrayAdapter.createFromResource(
            view.context,
            R.array.Taskpriority,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = adapter
        }
    }


    /**
     * Insert a SubTask in the database.
     * @param room
     * @param spnSubTask
     * @param taskId
     * @author Jessica Castillo
     */
    private fun insertSubTask(room: NoteFullDb, spnSubTask: Spinner, taskId: Int) {
        val subTaskName = binding.etSubTaskName
        val subTaskDescription = binding.etSubTaskDescription
        val spinnerContent = spnSubTask.selectedItem.toString()
        var spinnerId: Int = 0

        when (spinnerContent) {
            "Alta" -> spinnerId = 1
            "Media" -> spinnerId = 2
            "Baja" -> spinnerId = 3
        }

        val subtask = SubTask(
            subTaskName.text.toString(),
            subTaskDescription.text.toString(),
            taskId,
            spinnerId
        )

        lifecycleScope.launch {
            room.subTaskDao().insertSubTask(subtask)
        }


        Toast.makeText(
            context,
            "Subtarea insertada",
            Toast.LENGTH_LONG
        ).show()
    }

    private fun replaceFragment(fragment: Fragment) {
        requireActivity().supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentContainerView, fragment)
            commit()
        }
    }

}