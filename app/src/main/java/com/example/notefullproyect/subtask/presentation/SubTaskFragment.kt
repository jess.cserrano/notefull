package com.example.notefullproyect.subtask.presentation

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.notefullproyect.countdown.CountDownFragment
import com.example.notefullproyect.R
import com.example.notefullproyect.database.NoteFullDb
import com.example.notefullproyect.databinding.FragmentSubTaskBinding
import com.example.notefullproyect.subtask.model.SubTask
import com.example.notefullproyect.task.presentation.DeleteTaskFragment
import com.example.notefullproyect.task.presentation.RVTaskFragment
import com.example.notefullproyect.task.presentation.UpdateTaskFragment

/**
 * Implements the recyclerview, passing the list of days per taskId.
 * @author Jessica Castillo
 */
class SubTaskFragment : Fragment() {

    private var _binding: FragmentSubTaskBinding? = null
    private val binding get() = _binding!!

    private lateinit var builder: AlertDialog.Builder

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentSubTaskBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val room = NoteFullDb.getDatabase(view.context)

        val preferencesEditor = view.context.applicationContext.getSharedPreferences(
            "MySharedPreferences",
            AppCompatActivity.MODE_PRIVATE
        )
        val taskId = preferencesEditor.getInt("taskId", 0)

        val recyclerSublist = binding.rvSubListl
        val listSubtask = getAllSubTaskByTaskId(room, taskId)
        val adapter = SubtaskAdapter(listSubtask)
        recyclerSublist.layoutManager = LinearLayoutManager(view.context)
        recyclerSublist.adapter = adapter

        val task = room.taskDao().getTaskById(taskId)

        binding.tvNameTask.text = task.title
        binding.tvDescriptionTask.text = task.description

        /**
         *Configures the cardView item click by overriding the adapter method.
         * @author Jessica Castillo
         */
        adapter.setOnItemClickListener(object : SubtaskAdapter.OnItemClickListener {
            override fun onItemClick(position: Int) {

                val subtaskId = listSubtask[position].id

                val preferencesTask = requireContext().applicationContext.getSharedPreferences(
                    "MySharedPreferences",
                    AppCompatActivity.MODE_PRIVATE
                ).edit()
                preferencesTask.putInt("subtaskId", subtaskId)
                preferencesTask.apply()

                replaceFragment(SubTaskDetailFragment())
            }

            override fun onClick(v: View?) {
                TODO("Not yet implemented")
            }
        })


        binding.ivEditTask.setOnClickListener {

            replaceFragment(UpdateTaskFragment())
        }

        binding.ivPomodoroTask.setOnClickListener {
            replaceFragment(CountDownFragment())
        }

        binding.ivAddSubtask.setOnClickListener {
            replaceFragment(AddSubtaskFragment())
        }

        builder = AlertDialog.Builder(view.context)

        binding.ivDeleteTask.setOnClickListener {
            builder.setTitle("Borrar tarea")
                .setMessage("¿Seguro que quieres eliminar la tarea, puede contener subtareas?")
                .setCancelable(true)
                .setPositiveButton("Si") { dialogInterface, it ->
                    replaceFragment(DeleteTaskFragment())
                }
                .setNegativeButton("No") { dialogInterface, it ->
                    dialogInterface.cancel()
                }
                .show()
        }

        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {

                    replaceFragment(RVTaskFragment())

                }
            }
        )
    }

    private fun getAllSubTaskByTaskId(room: NoteFullDb, taskId: Int): List<SubTask> {

        return room.subTaskDao().getSubtasksByTaskId(taskId)
    }

    private fun replaceFragment(fragment: Fragment) {
        requireActivity().supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentContainerView, fragment)
            commit()
        }
    }
}