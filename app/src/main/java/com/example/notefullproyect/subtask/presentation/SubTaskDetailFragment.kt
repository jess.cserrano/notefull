package com.example.notefullproyect.subtask.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.notefullproyect.R
import com.example.notefullproyect.countdown.CountDownFragment
import com.example.notefullproyect.database.NoteFullDb
import com.example.notefullproyect.databinding.FragmentSubTaskDetailBinding

/**
 *Implements the detail view of the subtask.
 * @author Jessica Castillo
 */
class SubTaskDetailFragment : Fragment() {

    private var _binding: FragmentSubTaskDetailBinding? = null
    private val binding get() = _binding!!

    private lateinit var builder: AlertDialog.Builder


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSubTaskDetailBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val room = NoteFullDb.getDatabase(view.context)

        val preferencesEditor = view.context.applicationContext.getSharedPreferences(
            "MySharedPreferences",
            AppCompatActivity.MODE_PRIVATE
        )
        val subtaskId = preferencesEditor.getInt("subtaskId", 0)

        val name = binding.tvNameSubTask
        val edit = binding.ivBtnEditSub
        val description = binding.tvDescriptionSubTask


        val subtask = room.subTaskDao().getSubtaskById(subtaskId)
        name.text = subtask.title
        description.text = subtask.description

        edit.setOnClickListener {
            replaceFragment(UpdateSubTaskFragment())
        }

        binding.ivBtnPomodoroSub.setOnClickListener {
            replaceFragment(CountDownFragment())
        }
        builder = AlertDialog.Builder(view.context)
        binding.ivBtnDeleteSub.setOnClickListener {

            builder.setTitle("Borrar subtarea")
                .setMessage("¿Seguro que quieres eliminar la subtarea?")
                .setCancelable(true)
                .setPositiveButton("Si") { dialogInterface, it ->
                    replaceFragment(DeleteSubTaskFragment())
                }
                .setNegativeButton("No") { dialogInterface, it ->
                    dialogInterface.cancel()
                }
                .show()
        }

        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    val preferencesTask = requireContext().applicationContext.getSharedPreferences(
                        "MySharedPreferences",
                        AppCompatActivity.MODE_PRIVATE
                    ).edit()
                    preferencesTask.putInt("subtaskId", 0)
                    preferencesTask.apply()

                    replaceFragment(SubTaskFragment())
                }
            }
        )
    }

    private fun replaceFragment(fragment: Fragment) {
        requireActivity().supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentContainerView, fragment)
            commit()
        }
    }
}