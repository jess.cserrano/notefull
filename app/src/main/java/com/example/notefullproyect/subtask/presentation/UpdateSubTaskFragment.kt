package com.example.notefullproyect.subtask.presentation

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.example.notefullproyect.R
import com.example.notefullproyect.database.NoteFullDb
import com.example.notefullproyect.databinding.FragmentUpdateSubTaskBinding
import kotlinx.coroutines.launch

/**
 * Implements the functionality to update a SubTask in the database.
 * @author Jessica Castillo
 */
class UpdateSubTaskFragment : Fragment() {

    private var _binding: FragmentUpdateSubTaskBinding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentUpdateSubTaskBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val room = NoteFullDb.getDatabase(view.context)

        val preferencesEditor = view.context.applicationContext.getSharedPreferences(
            "MySharedPreferences",
            AppCompatActivity.MODE_PRIVATE
        )
        val subtaskId = preferencesEditor.getInt("subtaskId", 0)

        val name = binding.etSubTaskName
        val description = binding.etSubTaskDescription
        val spnPriority = binding.spnSubTask

        val subtask = room.subTaskDao().getSubtaskById(subtaskId)

        name.setText(subtask.title)
        description.setText(subtask.description)

        prioritySpinner(view, spnPriority, subtask.priorityId)

        binding.btnUpdateSubTask.setOnClickListener {

            lifecycleScope.launch {
                val subtask = room.subTaskDao().getSubtaskById(subtaskId)

                subtask.title = name.text.toString()
                subtask.description = description.text.toString()
                when (spnPriority.selectedItem) {
                    "Alta" -> subtask.priorityId = 1
                    "Media" -> subtask.priorityId = 2
                    "Baja" -> subtask.priorityId = 3
                }

                room.subTaskDao().updateSubTask(subtask)

                Toast.makeText(view.context, "Subtarea modificada", Toast.LENGTH_LONG).show()

                requireActivity().supportFragmentManager.beginTransaction().apply {
                    replace(R.id.fragmentContainerView, SubTaskDetailFragment())
                    commit()
                }

            }

        }

        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {

                    requireActivity().supportFragmentManager.beginTransaction().apply {
                        replace(R.id.fragmentContainerView, SubTaskDetailFragment())
                        commit()
                    }

                }
            }
        )


    }

    private fun prioritySpinner(view: View, spinner: Spinner, priorityId: Int) {
        val priority = resources.getStringArray(R.array.Taskpriority)

        ArrayAdapter.createFromResource(
            view.context,
            R.array.Taskpriority,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = adapter
            when (priorityId) {
                1 -> spinner.setSelection(2)
                2 -> spinner.setSelection(1)
                3 -> spinner.setSelection(0)
            }
        }
    }

}