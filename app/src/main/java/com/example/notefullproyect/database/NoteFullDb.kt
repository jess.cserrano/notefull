package com.example.notefullproyect.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.notefullproyect.priority.data.PriorityDao
import com.example.notefullproyect.progress.data.ProgressDao
import com.example.notefullproyect.subtask.data.SubTaskDao
import com.example.notefullproyect.task.data.TaskDao
import com.example.notefullproyect.priority.model.Priority
import com.example.notefullproyect.progress.model.Progress
import com.example.notefullproyect.subtask.model.SubTask
import com.example.notefullproyect.task.model.Task
import com.example.notefullproyect.user.data.UserDao
import com.example.notefullproyect.user.model.User

/**
 * NoteFullDb Database entities.
 * Contains the five database's entities: User, Task, Priority, Progress and SubTask
 * @author Jessica Castillo
 */
@Database(
    entities = [
        User::class, Task::class, Priority::class, Progress::class, SubTask::class],
    version = 1
)

/**
 * Configure the database and define the Daos that can access it.
 * @author Jessica Castillo
 */
abstract class NoteFullDb : RoomDatabase() {

    abstract fun userDao(): UserDao
    abstract fun taskDao(): TaskDao
    abstract fun priorityDao(): PriorityDao
    abstract fun progressDao(): ProgressDao
    abstract fun subTaskDao(): SubTaskDao

    companion object {
        @Volatile
        private var INSTANCE: NoteFullDb? = null

        /**
         *Get a connection to the database.
         * @return if it is configured it returns a valid NoteFullDb.If it is not returns a new NoteFullDb configuration.
         * @author Jessica Castillo
         */
        fun getDatabase(context: Context): NoteFullDb {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }

            synchronized(this) {
                val instance = Room
                    .databaseBuilder(context, NoteFullDb::class.java, "noteFull")
                    .allowMainThreadQueries()
                    .build()
                INSTANCE = instance
                return instance
            }
        }
    }
}
