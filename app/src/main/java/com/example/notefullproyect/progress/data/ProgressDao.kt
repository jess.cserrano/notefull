package com.example.notefullproyect.progress.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.notefullproyect.progress.model.Progress

/**
 * Functions that allow communication with the database for Progress entity.
 * @author Jessica Castillo
 */
@Dao
interface ProgressDao {

    @Insert
    fun insertProgressValues(progress: List<Progress>)

    @Query("SELECT * FROM Progress")
    fun getProgressValues(): List<Progress>
}