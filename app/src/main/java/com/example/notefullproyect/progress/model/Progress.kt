package com.example.notefullproyect.progress.model

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Progress entity with an autogenerated id.
 * @constructor id,description.
 * @author Jessica Castillo
 */
@Entity
data class Progress(
    @PrimaryKey
    var id: Int,
    var description: String
)




