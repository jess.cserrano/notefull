package com.example.notefullproyect.countdown

import android.app.Activity
import android.content.Context
import android.widget.TextView
import java.util.*

/**
 * Adapts milliseconds in a minutes and seconds format.
 */
class FormatText(context: Context, textTime: Int) {

    var textTime: TextView = (context as Activity).findViewById(textTime)

    /**
     * Gets the timeLeftInMillis and converts it to minutes & seconds for printing it.
     */
    fun countDownText(timeLeftInMillis: Long) {
        val minutes: Int = (timeLeftInMillis.toInt() / 1000) / 60
        val seconds: Int = (timeLeftInMillis.toInt() / 1000) % 60
        val timeLeftFormatted: String =
            java.lang.String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds)
        textTime.text = timeLeftFormatted
    }
}