package com.example.notefullproyect.countdown

import android.media.MediaPlayer
import android.os.Bundle
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.example.notefullproyect.R
import com.example.notefullproyect.database.NoteFullDb
import com.example.notefullproyect.databinding.FragmentCountDownBinding
import com.example.notefullproyect.subtask.presentation.SubTaskDetailFragment
import com.example.notefullproyect.subtask.presentation.SubTaskFragment
import kotlinx.coroutines.launch

/**
 *implements a 25 min countdown for use to focus in a task or subtask.
 * @author Jessica Castillo
 */
open class CountDownFragment : Fragment() {

    private var _binding: FragmentCountDownBinding? = null
    private val binding get() = _binding!!

    var formatText: FormatText? = null

    private lateinit var progressB: ProgressBar
    private lateinit var ivPlayPause: ImageView

    private var pomodoro: MediaPlayer? = null


    private lateinit var timerStatus: TimerStatus
    var timeLeftInMillis: Long = 0
    lateinit var countDownTimer: CountDownTimer

    lateinit var room: NoteFullDb

    var taskId: Int = 0
    var subtaskId: Int = 0


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCountDownBinding.inflate(inflater, container, false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        room = NoteFullDb.getDatabase(view.context)

        val preferencesTask = view.context.applicationContext.getSharedPreferences(
            "MySharedPreferences",
            AppCompatActivity.MODE_PRIVATE
        )
        taskId = preferencesTask.getInt("taskId", 0)
        subtaskId = preferencesTask.getInt("subtaskId", 0)


        ivPlayPause = binding.ivPlayPause
        progressB = binding.pbarCountDown

        timerStatus = TimerStatus.STOPPED

        timeLeftInMillis = (1 * 60000).toLong()

        initTimer()

        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    stopSound()
                    val builder = AlertDialog.Builder(requireContext())
                    builder.setTitle("Abandonar tiempo de concentración")
                        .setMessage("¿Seguro que quieres abandonar?")
                        .setCancelable(true)
                        .setPositiveButton("Si") { dialogInterface, it ->

                            if (subtaskId > 0) {
                                replaceFragment(SubTaskDetailFragment())
                            } else {
                                replaceFragment(SubTaskFragment())
                            }

                        }
                        .setNegativeButton("No") { dialogInterface, it ->
                            dialogInterface.cancel()
                        }
                        .show()
                }
            }
        )
    }

    /**
     * Init timer. It creates a timer in milliseconds.
     */
    private fun initTimer() {

        formatText = view?.let { FormatText(it.context, R.id.tvTimeCountDown) }

        ivPlayPause.setOnClickListener {
            stopSound()
            if (timerStatus == TimerStatus.STOPPED) {
                start()
            } else {
                pause()
            }
        }

        binding.ivReplay.setOnClickListener {
            reset()
            stopSound()
        }

        formatText!!.countDownText(timeLeftInMillis)
        setProgressValues()
    }

    private fun setProgressValues() {
        progressB.max = (timeLeftInMillis.toInt() / 1000)
        progressB.progress = timeLeftInMillis.toInt() / 1000

    }

    private fun reset() {
        pause()
        timeLeftInMillis = (1 * 60000).toLong()
        setProgressValues()
        formatText!!.countDownText(timeLeftInMillis)
    }

    private fun start() {
        countDownTimer = object : CountDownTimer(timeLeftInMillis, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                timeLeftInMillis = millisUntilFinished
                ivPlayPause.setImageResource(R.drawable.ic_pause)
                formatText!!.countDownText(timeLeftInMillis)
                progressB.progress = (millisUntilFinished / 1000).toInt()
            }

            override fun onFinish() {

                soundPomodoro(view)

                timerStatus = TimerStatus.STOPPED
                reset()
                replaceFragment(BreakFragment())

                updatePomodoroTask()
                if (subtaskId > 0) {
                    updatePomodoroSubTask()
                }
            }
        }.start()

        timerStatus = TimerStatus.STARTED
    }

    private fun pause() {
        countDownTimer.cancel()
        timerStatus = TimerStatus.STOPPED
        ivPlayPause.setImageResource(R.drawable.ic_play)

    }

    private fun replaceFragment(fragment: Fragment) {
        requireActivity().supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentContainerView, fragment)
            commit()
        }
    }

    private fun updatePomodoroTask() {

        val currentTask = room.taskDao().getTaskById(taskId)
        currentTask.quantityTime = currentTask.quantityTime + 1
        lifecycleScope.launch {
            room.taskDao().updateTask(currentTask)
        }
    }

    private fun updatePomodoroSubTask() {

        val subtask = room.subTaskDao().getSubtasksByTaskIdAndSubtaskId(taskId, subtaskId)
        subtask.quantityTime = subtask.quantityTime + 1

        lifecycleScope.launch {
            room.subTaskDao().updateSubTask(subtask)
        }
    }

    fun soundPomodoro(view: View?) {
        if (view != null) {
            pomodoro = MediaPlayer.create(view.context, R.raw.pomo)
        }
        pomodoro?.start()
    }

    fun stopSound() {
        if (pomodoro != null && pomodoro!!.isPlaying) {
            pomodoro!!.stop()
        }
    }

}