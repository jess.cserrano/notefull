package com.example.notefullproyect.countdown

import android.media.MediaPlayer
import android.os.Bundle
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.example.notefullproyect.R
import com.example.notefullproyect.databinding.FragmentBreakBinding

/**
 *Implements a 5 min countdown for a break in a task or subtask.
 * @author Jessica Castillo
 */
class BreakFragment : Fragment(), View.OnClickListener {

    private var _binding: FragmentBreakBinding? = null
    private val binding get() = _binding!!

    private lateinit var timerStatus: TimerStatus
    lateinit var countDownTimer: CountDownTimer

    private var breakPomodoro: MediaPlayer? = null

    var formatText: FormatText? = null

    var timeLeftInMillis: Long = (5 * 60000).toLong()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentBreakBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        timerStatus = TimerStatus.STOPPED

        val btnBreak = binding.btnBreak
        formatText = FormatText(view.context, R.id.tvHourBreak)
        formatText!!.countDownText(timeLeftInMillis)

        binding.ivPlayBreak.setOnClickListener {
            startBreak()
            stopSound(view)
        }

        btnBreak.setOnClickListener { this }

        binding.btnBreak.setOnClickListener {
            replaceFragment(CountDownFragment())
            stopSound(view)
        }

        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    replaceFragment(CountDownFragment())
                }
            }
        )
    }

    private fun startBreak() {
        countDownTimer = object : CountDownTimer(timeLeftInMillis, 1000) {
            override fun onTick(millisUltinFin: Long) {
                timeLeftInMillis = millisUltinFin
                formatText!!.countDownText(timeLeftInMillis)
            }

            override fun onFinish() {

                timerStatus = TimerStatus.STOPPED

                soundBreakPomodoro(view)
                Toast.makeText(context, "SE ACABÓ", Toast.LENGTH_SHORT).show()
                replaceFragment(CountDownFragment())
            }
        }.start()
        timerStatus = TimerStatus.STARTED
    }

    private fun pauseBreak() {
        countDownTimer.cancel()
        timerStatus = TimerStatus.STOPPED
    }

    override fun onClick(view: View?) {
        when (requireView().id) {
            R.id.btnBreak -> {
                if (timerStatus == TimerStatus.STARTED) {
                    pauseBreak()

                    val builder = AlertDialog.Builder(requireContext())
                    builder.setTitle("Borrar subtarea")
                        .setMessage("¿Seguro que quieres eliminar la subtarea?")
                        .setCancelable(true)
                        .setPositiveButton("Si") { dialogInterface, it ->
                            Toast.makeText(context, "PRUEBA", Toast.LENGTH_SHORT).show()
                        }
                        .setNegativeButton("No") { dialogInterface, it ->
                            dialogInterface.cancel()
                        }
                        .show()
                }
            }
        }
    }

    private fun replaceFragment(fragment: Fragment) {
        requireActivity().supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentContainerView, fragment)
            commit()
        }
    }

    fun soundBreakPomodoro(view: View?) {
        if (view != null) {
            breakPomodoro = MediaPlayer.create(view.context, R.raw.break_pomodoro)
        }
        breakPomodoro?.start()
    }

    private fun stopSound(view: View?) {
        if (breakPomodoro != null && breakPomodoro!!.isPlaying) {
            breakPomodoro!!.stop()
        }
    }

}