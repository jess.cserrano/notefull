package com.example.notefullproyect.countdown

/**
 * Enum of possible Countdown status.
 * @author Jessica Castillo
 */
enum class TimerStatus {
    STARTED, STOPPED
}