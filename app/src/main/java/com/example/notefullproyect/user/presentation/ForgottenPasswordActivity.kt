package com.example.notefullproyect.user.presentation

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Patterns
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import com.example.notefullproyect.MainActivity
import com.example.notefullproyect.database.NoteFullDb
import com.example.notefullproyect.databinding.ActivityForgottenPasswordBinding
import kotlinx.coroutines.launch

/**
 * Changes the password in case of forgetting.
 * @author Jessica Castillo
 */
class ForgottenPasswordActivity : AppCompatActivity() {

    private lateinit var binding: ActivityForgottenPasswordBinding
    private lateinit var room: NoteFullDb

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityForgottenPasswordBinding.inflate(layoutInflater)
        setContentView(binding.root)

        room = NoteFullDb.getDatabase(applicationContext)

        val userEmail = binding.etUserEmail.text
        val newPassword = binding.etActualPassword.text
        val newPassword2 = binding.etActualPassword2.text

        binding.btnNewPassword.setOnClickListener {
            if (userEmail != null && userEmail.toString()
                    .isNotEmpty() && newPassword != null && newPassword.toString()
                    .isNotEmpty() && newPassword2 != null && newPassword2.toString().isNotEmpty()
            ) {
                if (!isValidEmail(userEmail.toString())) {
                    Toast.makeText(
                        applicationContext,
                        "El correo no tiene un formato válido.",
                        Toast.LENGTH_LONG
                    ).show()

                    return@setOnClickListener
                } else {
                    val user = room.userDao().getUserByEmail(userEmail.toString())
                    if (user != null) {
                        if (newPassword.toString() == newPassword2.toString()) {
                            user.password = newPassword.toString()

                            lifecycleScope.launch {
                                room.userDao().update(user)
                            }

                            Toast.makeText(
                                applicationContext,
                                "Se ha actualizado la contraseña.",
                                Toast.LENGTH_LONG
                            ).show()

                            startActivity(Intent(applicationContext, MainActivity::class.java))

                        } else {
                            Toast.makeText(
                                applicationContext,
                                "Las contraseñas no coinciden.",
                                Toast.LENGTH_LONG
                            ).show()

                            return@setOnClickListener
                        }
                    } else {
                        Toast.makeText(
                            applicationContext,
                            "El correo no hace referencia a ningún usuario.",
                            Toast.LENGTH_LONG
                        ).show()

                        return@setOnClickListener
                    }

                }

            } else {
                Toast.makeText(
                    applicationContext,
                    "Ninguno de los campos puede estar en blanco.",
                    Toast.LENGTH_LONG
                ).show()

                return@setOnClickListener
            }
        }
    }

    /**
     * Method provided by android to know if an email is valid or not.
     */
    private fun isValidEmail(email: CharSequence?): Boolean {
        return if (TextUtils.isEmpty(email)) {
            false
        } else {
            Patterns.EMAIL_ADDRESS.matcher(email).matches()
        }
    }
}