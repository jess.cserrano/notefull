package com.example.notefullproyect.user.presentation

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.example.notefullproyect.R
import com.example.notefullproyect.database.NoteFullDb
import com.example.notefullproyect.databinding.FragmentUserSettingsBinding
import com.example.notefullproyect.task.presentation.RVTaskFragment
import kotlinx.coroutines.launch

/***
 * Implements user name and password modifications
 * @author Jessica Castillo
 */
class UserSettingsFragment : Fragment() {

    private var _binding: FragmentUserSettingsBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentUserSettingsBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val room = NoteFullDb.getDatabase(view.context)

        val userName = binding.etNewUserName.text
        val actualPassword = binding.etActualPassword.text
        val newPassword = binding.etNewPassword.text
        val newPassword2 = binding.etRepeatNewPassword.text


        val preferencesEditor = view.context.applicationContext.getSharedPreferences(
            "MySharedPreferences",
            AppCompatActivity.MODE_PRIVATE
        )
        val userId = preferencesEditor.getInt("userId", 0)

        val user = room.userDao().getUserById(userId)

        binding.btnUpdateName.setOnClickListener {
            if (userName != null && userName.isNotEmpty()) {

                user.name = userName.toString()
                lifecycleScope.launch {
                    room.userDao().update(user)
                }
                Toast.makeText(
                    view.context,
                    "El nombre se ha modificado.",
                    Toast.LENGTH_LONG
                ).show()
            } else {
                Toast.makeText(
                    view.context,
                    "El nombre está vacío.",
                    Toast.LENGTH_LONG
                ).show()
                return@setOnClickListener
            }
        }

        binding.btnUpdatePassword.setOnClickListener {

            if (actualPassword != null && actualPassword.isNotEmpty()
                && newPassword != null && newPassword.isNotEmpty()
                && newPassword2 != null && newPassword2.isNotEmpty()
            ) {
                if (user.password == actualPassword.toString()) {
                    if (newPassword.toString() == newPassword2.toString()) {
                        user.password = newPassword.toString()
                        lifecycleScope.launch {
                            room.userDao().update(user)
                        }
                        Toast.makeText(
                            view.context,
                            "La contraseña se ha modificado.",
                            Toast.LENGTH_LONG
                        ).show()
                    } else {
                        Toast.makeText(
                            view.context,
                            "Las contraseñas no coinciden.",
                            Toast.LENGTH_LONG
                        ).show()
                        return@setOnClickListener
                    }
                } else {
                    Toast.makeText(
                        view.context,
                        "La contraseña actual no coincide.",
                        Toast.LENGTH_LONG
                    ).show()
                    return@setOnClickListener
                }
            } else {
                Toast.makeText(
                    view.context,
                    "Las contraseñas no pueden estar vacías.",
                    Toast.LENGTH_LONG
                ).show()
                return@setOnClickListener

            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    replaceFragment(RVTaskFragment())
                }
            }
        )
    }

    private fun replaceFragment(fragment: Fragment) {
        requireActivity().supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentContainerView, fragment)
            commit()
        }
    }
}