package com.example.notefullproyect.user.model

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Implements user data into our project. It also refers to the table users.
 */
@Entity
data class User(
    var name: String,
    var email: String,
    var password: String
) {
    @PrimaryKey(autoGenerate = true) var id: Int = 0
}

