package com.example.notefullproyect.user.data

import androidx.room.*
import com.example.notefullproyect.user.model.User

/**
 * Implements data access object for Users.
 */
@Dao
interface UserDao {

    @Query("SELECT * FROM User ORDER BY id ASC")
    suspend fun getAllUsers(): List<User>

    @Query("SELECT * FROM User WHERE email = :email")
    fun getUserByEmail(email: String): User

    @Query("SELECT * FROM User WHERE id = :id")
    fun getUserById(id: Int): User

    @Update
    suspend fun update(user: User)

    @Insert
    suspend fun insertUser(user: User)

    @Delete
    suspend fun delete(user: User)
}