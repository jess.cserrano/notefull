package com.example.notefullproyect.user.presentation

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Patterns
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.example.notefullproyect.MainActivity
import com.example.notefullproyect.database.NoteFullDb
import com.example.notefullproyect.databinding.ActivitySignInBinding
import com.example.notefullproyect.user.model.User
import kotlinx.coroutines.launch


class SignInActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySignInBinding
    private lateinit var room: NoteFullDb

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySignInBinding.inflate(layoutInflater)
        setContentView(binding.root)

        room = NoteFullDb.getDatabase(applicationContext)

        val userName = binding.etRegisterName
        val userEmail = binding.etRegisterEmail
        val userPassword = binding.etRegisterPassword
        val userRepeatPassword = binding.etRegisterRepeatPassword


        binding.btnRegister.setOnClickListener {

            if (userName.text.toString().isEmpty() || userEmail.text.toString()
                    .isEmpty() || userPassword.text.toString()
                    .isEmpty() || userRepeatPassword.text.toString().isEmpty()
            ) {
                Toast.makeText(
                    applicationContext,
                    "Ninguno de los campos puede estar en blanco.",
                    Toast.LENGTH_LONG
                ).show()

                return@setOnClickListener
            }

            if (!isValidEmail(userEmail.text)) {
                Toast.makeText(
                    applicationContext,
                    "El correo no tiene un formato válido.",
                    Toast.LENGTH_LONG
                ).show()

                return@setOnClickListener
            }
            var user =
                User(
                    userName.text.toString(),
                    userEmail.text.toString(),
                    userPassword.text.toString()
                )

            val userFromDB = room.userDao().getUserByEmail(user.email)
            if (userFromDB != null) {
                /**El usuario existe */
                Toast.makeText(
                    applicationContext,
                    "Ese correo ya existe y está asociado a un usuario.",
                    Toast.LENGTH_LONG
                ).show()
                return@setOnClickListener

            }
            /**El usuario no existe*/
            if (userPassword.text.toString() == userRepeatPassword.text.toString()) {

                lifecycleScope.launch {
                    room.userDao().insertUser(user)
                }

                Toast.makeText(applicationContext, "Usuario registrado", Toast.LENGTH_LONG)
                    .show()

                startActivity(Intent(applicationContext, MainActivity::class.java))

            } else {
                Toast.makeText(
                    applicationContext,
                    "Las contraseñas no coinciden",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    /**
     * Tests if an email is correct or not
     */
    private fun isValidEmail(email: CharSequence?): Boolean {
        return if (TextUtils.isEmpty(email)) {
            false
        } else {
            Patterns.EMAIL_ADDRESS.matcher(email).matches()
        }
    }

}
