package com.example.notefullproyect.time

import android.app.DatePickerDialog
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.widget.DatePicker
import androidx.fragment.app.DialogFragment
import com.example.notefullproyect.HomeActivity
import java.util.*

/**
 * Implements a valid Calendar.
 * @author Jessica Castillo
 */
class DatePickerFragment(val listener: (year: Int, month: Int, day: Int) -> Unit) :
    DialogFragment(), DatePickerDialog.OnDateSetListener {

    private val calendar = Calendar.getInstance()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)

        val dialog = DatePickerDialog(requireActivity(), this, year, month, day)

        dialog.setButton(
            DialogInterface.BUTTON_NEGATIVE, "Cancel"
        ) { dialog, which ->
            if (which == DialogInterface.BUTTON_NEGATIVE) {
                dialog.dismiss()
                onBackPressed()
            }
        }

        return dialog
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        calendar.set(Calendar.YEAR, year)
        calendar.set(Calendar.MONTH, month)
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        listener(year, month, dayOfMonth)

    }

    fun onBackPressed() {

        val intent = Intent (activity, HomeActivity::class.java)
        activity?.startActivity(intent)
    }

}