package com.example.notefullproyect.time

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.example.notefullproyect.R
import com.example.notefullproyect.task.presentation.RVTaskFragment
import java.text.SimpleDateFormat
import java.util.*

/**
 * Contains a DatePickerFragment and ok function to call an another fragment.
 * @author Jessica Castillo
 */
class CalendarFragment : Fragment() {

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val datePickerFragment =
            DatePickerFragment { year, month, day -> onDateSelected(year, month, day) }

        val supportFragmentManager = requireActivity().supportFragmentManager
        datePickerFragment.show(supportFragmentManager, "datePicker")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_calendar, container, false)
    }

    @SuppressLint("SimpleDateFormat")
    @RequiresApi(Build.VERSION_CODES.O)
    private fun onDateSelected(year: Int, month: Int, day: Int) {

        val cal = Calendar.getInstance()
        cal[Calendar.YEAR] = year
        cal[Calendar.MONTH] = month
        cal[Calendar.DAY_OF_MONTH] = day
        val sdf = SimpleDateFormat("dd/MM/yyyy")
        val date = sdf.format(cal.time)

        val bundle = Bundle()
        bundle.putString("dateFromCalendar", date)
        val fragment = RVTaskFragment()
        fragment.arguments = bundle

        requireActivity().supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentContainerView, fragment)
            commit()
        }
    }

}