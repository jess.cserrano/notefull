package com.example.notefullproyect.task.presentation

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.example.notefullproyect.R
import com.example.notefullproyect.database.NoteFullDb
import com.example.notefullproyect.databinding.FragmentDeleteTaskBinding
import kotlinx.coroutines.launch

/**
 * Implements the functionality to delete a task in the database.
 * @author Jessica Castillo
 */
class DeleteTaskFragment : Fragment() {

    private var _binding: FragmentDeleteTaskBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDeleteTaskBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val room = NoteFullDb.getDatabase(view.context)

        val preferencesEditor = view.context.applicationContext.getSharedPreferences(
            "MySharedPreferences",
            AppCompatActivity.MODE_PRIVATE
        )
        val taskId = preferencesEditor.getInt("taskId", 0)

        deleteTask(room, taskId)

    }

    /**
     * Delete a Task in the database.
     * @param room
     * @param taskId
     * @author Jessica Castillo
     */
    private fun deleteTask(room: NoteFullDb, taskId: Int) {

        lifecycleScope.launch {
            room.subTaskDao().deleteSubTasksByTaskId(taskId)
        }

        lifecycleScope.launch {
            room.taskDao().deleteTaskById(taskId)
        }

        Toast.makeText(context, "La tarea se ha eliminado correctamente", Toast.LENGTH_SHORT).show()

        replaceFragment(RVTaskFragment())
    }

    private fun replaceFragment(fragment: Fragment) {
        requireActivity().supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentContainerView, fragment)
            commit()
        }
    }

}