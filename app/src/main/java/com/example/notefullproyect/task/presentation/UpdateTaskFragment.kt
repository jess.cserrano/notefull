package com.example.notefullproyect.task.presentation

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.example.notefullproyect.R
import com.example.notefullproyect.subtask.presentation.SubTaskFragment
import com.example.notefullproyect.database.NoteFullDb
import com.example.notefullproyect.databinding.FragmentUpdateTaskBinding
import com.example.notefullproyect.time.TimePickerHelper
import kotlinx.coroutines.launch
import java.util.*

/**
 * Implements the functionality to update a Task in the database.
 * @author Jessica Castillo
 */
class UpdateTaskFragment : Fragment() {

    private var _binding: FragmentUpdateTaskBinding? = null
    private val binding get() = _binding!!

    lateinit var timePicker: TimePickerHelper


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentUpdateTaskBinding.inflate(inflater, container, false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val room = NoteFullDb.getDatabase(view.context)

        val preferencesEditor = view.context.applicationContext.getSharedPreferences(
            "MySharedPreferences",
            AppCompatActivity.MODE_PRIVATE
        )
        val taskId = preferencesEditor.getInt("taskId", 0)

        val spn = binding.spnPriority
        val name = binding.etUpdateTaskName
        val description = binding.etUpdateDescription
        val hour = binding.tvUpdateTaskHour


        timePicker = TimePickerHelper(view.context, false, true)

        binding.btnSelectTaskHour.setOnClickListener {
            showTimePickerDialog()
        }

        setValuesTask(room, taskId, name, description, spn, hour, view)

        binding.btnUpdateTask.setOnClickListener {

            updateTask(room, taskId, name, description, spn, hour)

            replaceFragment(SubTaskFragment())
        }

        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    replaceFragment(SubTaskFragment())
                }
            }
        )
    }

    private fun prioritySpinner(view: View, spinner: Spinner, priorityId:Int) {

        ArrayAdapter.createFromResource(
            view.context,
            R.array.Taskpriority,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = adapter
            when (priorityId) {
                1 -> spinner.setSelection(2)
                2 -> spinner.setSelection(1)
                3 -> spinner.setSelection(0)
            }
        }
    }

    private fun showTimePickerDialog() {
        val calendar = Calendar.getInstance()
        val hour = calendar.get(Calendar.HOUR_OF_DAY)
        val minute = calendar.get(Calendar.MINUTE)
        timePicker.showDialog(hour, minute, object : TimePickerHelper.Callback {
            @SuppressLint("SetTextI18n")
            override fun onTimeSelected(hourOfDay: Int, minute: Int) {
                val hourStr = if (hourOfDay < 10) "0${hourOfDay}" else "$hourOfDay"
                val minuteStr = if (minute < 10) "0${minute}" else "$minute"
                binding.tvUpdateTaskHour.text = "${hourStr} : ${minuteStr}"
            }
        })
    }

    /**
     * sets the current values of the task.
     * @param room
     * @param taskId
     * @param name
     * @param description
     * @param spinner
     * @param hour
     * @param view
     * @author Jessica Castillo
     */
    private fun setValuesTask(
        room: NoteFullDb,
        taskId: Int,
        name: EditText,
        description: EditText,
        spinner: Spinner,
        hour: TextView,
        view: View
    ) {
        val task = room.taskDao().getTaskById(taskId)

        name.setText(task.title)
        description.setText(task.description)
        hour.text = task.hour


        prioritySpinner(view, spinner, task.priorityId)
    }

    /**
     * updates a task in the database
     * @param room
     * @param taskId
     * @param name
     * @param description
     * @param spn
     * @param hour
     * @author Jessica Castillo.
     */
    private fun updateTask(
        room: NoteFullDb,
        taskId: Int,
        name: EditText,
        description: EditText,
        spn: Spinner,
        hour: TextView
    ) {

        val task = room.taskDao().getTaskById(taskId)

        task.title = name.text.toString()
        task.description = description.text.toString()
        task.hour = hour.text.toString()

        when (spn.selectedItem) {
            "Alta" -> task.priorityId = 1
            "Media" -> task.priorityId = 2
            "Baja" -> task.priorityId = 3
        }


        lifecycleScope.launch {
            room.taskDao().updateTask(task)
        }

        Toast.makeText(context, "Tarea modificada", Toast.LENGTH_LONG).show()
    }

    private fun replaceFragment(fragment: Fragment) {
        requireActivity().supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentContainerView, fragment)
            commit()
        }
    }

}