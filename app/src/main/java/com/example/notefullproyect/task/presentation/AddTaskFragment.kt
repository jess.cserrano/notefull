package com.example.notefullproyect.task.presentation

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.TextView
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.example.notefullproyect.R
import com.example.notefullproyect.database.NoteFullDb
import com.example.notefullproyect.databinding.FragmentAddTaskBinding
import com.example.notefullproyect.task.model.Task
import com.example.notefullproyect.time.TimePickerHelper
import kotlinx.coroutines.launch
import java.util.*

/**
 * Implements the functionality to add a task in the database.
 * @author Jessica Castillo
 */
class AddTaskFragment : Fragment() {

    private var _binding: FragmentAddTaskBinding? = null
    private val binding get() = _binding!!

    lateinit var timePicker: TimePickerHelper

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentAddTaskBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val room = NoteFullDb.getDatabase(view.context)

        val preferencesEditor = view.context.applicationContext.getSharedPreferences(
            "MySharedPreferences",
            AppCompatActivity.MODE_PRIVATE
        )
        val currentDate = preferencesEditor.getString("currentDate", "")
        val userId = preferencesEditor.getInt("userId", 0)

        val textHour = binding.tvHourTask
        val spinner = binding.spnPriority
        val btnAddTask = binding.btnAddTask

        btnAddTask.setOnClickListener {

            if (binding.etTaskName.text.isBlank()) {
                Toast.makeText(
                    context,
                    "Se debe introducir un valor para el campo nombre",
                    Toast.LENGTH_LONG
                ).show()
            } else {

                insertTask(room, spinner, userId, currentDate!!, textHour)
                replaceFragment(RVTaskFragment())
            }
        }

        setHour(textHour)

        timePicker = TimePickerHelper(view.context, false, true)

        binding.btnSelectTaskHour.setOnClickListener {
            showTimePickerDialog()
        }

        prioritySpinner(view, spinner)

        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    replaceFragment(RVTaskFragment())
                }
            }
        )
    }

    private fun showTimePickerDialog() {
        val calendar = Calendar.getInstance()
        val hour = calendar.get(Calendar.HOUR_OF_DAY)
        val minute = calendar.get(Calendar.MINUTE)
        timePicker.showDialog(hour, minute, object : TimePickerHelper.Callback {
            @SuppressLint("SetTextI18n")
            override fun onTimeSelected(hourOfDay: Int, minute: Int) {
                val hourStr = if (hourOfDay < 10) "0${hourOfDay}" else "$hourOfDay"
                val minuteStr = if (minute < 10) "0${minute}" else "$minute"
                binding.tvHourTask.text = "${hourStr} : ${minuteStr}"
            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun setHour(textHour: TextView) {
        val cal = Calendar.getInstance()
        val actualHour = cal.get(Calendar.HOUR_OF_DAY)
        val actualMinute = cal.get(Calendar.MINUTE)
        if (actualMinute < 10) {
            textHour.text = "$actualHour : 0$actualMinute"
        } else textHour.text = "$actualHour : $actualMinute"
    }

    private fun prioritySpinner(view: View, spinner: Spinner) {
        ArrayAdapter.createFromResource(
            view.context,
            R.array.Taskpriority,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = adapter
        }
    }

    /**
     * Insert a Task in the database.
     * @param room
     * @param spinner
     * @param userId
     * @param currentDate
     * @param textHour
     * @author Jessica Castillo
     */
    private fun insertTask(
        room: NoteFullDb,
        spinner: Spinner,
        userId: Int,
        currentDate: String,
        textHour: TextView
    ) {

        val etTaskName = binding.etTaskName
        val etDescription = binding.etDescription
        val spinnerContent = spinner.selectedItem.toString()
        var priorityId: Int = 0

        when (spinnerContent) {
            "Alta" -> priorityId = 1
            "Media" -> priorityId = 2
            "Baja" -> priorityId = 3
        }

        val task = Task(
            userId,
            etTaskName.text.toString(),
            etDescription.text.toString(),
            currentDate,
            textHour.text.toString(),
            priorityId
        )

        lifecycleScope.launch {
            room.taskDao().insertTask(task)
        }

        Toast.makeText(
            context,
            "Tarea insertada",
            Toast.LENGTH_LONG
        ).show()

    }

    private fun replaceFragment(fragment: Fragment) {
        requireActivity().supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentContainerView, fragment)
            commit()
        }
    }

}