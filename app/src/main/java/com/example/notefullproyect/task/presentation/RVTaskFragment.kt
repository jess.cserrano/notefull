package com.example.notefullproyect.task.presentation

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.notefullproyect.R
import com.example.notefullproyect.subtask.presentation.SubTaskFragment
import com.example.notefullproyect.database.NoteFullDb
import com.example.notefullproyect.databinding.FragmentRVTaskBinding
import com.example.notefullproyect.task.model.Task
import java.text.SimpleDateFormat
import java.util.*

/**
 * Implements the recyclerview, passing the list of days per user and day to the adapter.
 * @author Jessica Castillo
 */
class RVTaskFragment : Fragment() {

    private var _binding: FragmentRVTaskBinding? = null
    private val binding get() = _binding!!
    private lateinit var currentDate: String


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentRVTaskBinding.inflate(inflater, container, false)

        return binding.root

    }

    @SuppressLint("CommitPrefEdits", "SetTextI18n", "SimpleDateFormat")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val room = NoteFullDb.getDatabase(view.context)


        val preferencesEditor = view.context.applicationContext.getSharedPreferences(
            "MySharedPreferences",
            AppCompatActivity.MODE_PRIVATE
        )
        val userId = preferencesEditor.getInt("userId", 0)
        val date = preferencesEditor.getString("currentDate", "")

        val args = this.arguments

        if (args != null) {
            currentDate = args.getString("dateFromCalendar")!!
        } else {
            currentDate = if (date != null && date.isNotBlank() && date.isNotEmpty()) {
                date
            } else {
                val sdf = SimpleDateFormat("dd/MM/yyyy")
                sdf.format(Date())
            }
        }

        binding.tvDate.text = "Tareas para  $currentDate"

        val preferencesCurrentDate = view.context.getSharedPreferences(
            "MySharedPreferences",
            AppCompatActivity.MODE_PRIVATE
        ).edit()
        preferencesCurrentDate.putString("currentDate", currentDate)
        preferencesCurrentDate.apply()

        val recyclerView = binding.rvList
        val listTask = getAllTaskByUserIdAndDate(room, userId, currentDate)
        val adapter = TaskAdapter(listTask)
        recyclerView.layoutManager = LinearLayoutManager(view.context)
        recyclerView.adapter = adapter

        /**
         *Configures the cardView item click by overriding the adapter method.
         * @author Jessica Castillo
         */
        adapter.setOnItemClickListener(object : TaskAdapter.OnItemClickListener {
            override fun onItemClick(position: Int) {

                val taskId = listTask[position].id

                val preferencesTask = requireContext().applicationContext.getSharedPreferences(
                    "MySharedPreferences",
                    AppCompatActivity.MODE_PRIVATE
                ).edit()
                preferencesTask.putInt("taskId", taskId)
                preferencesTask.apply()

                replaceFragment(SubTaskFragment())
            }

            override fun onClick(v: View?) {
                TODO("Not yet implemented")
            }

        })

        binding.imageViewAddTask.setOnClickListener {

            replaceFragment(AddTaskFragment())
        }
    }

    private fun getAllTaskByUserIdAndDate(
        room: NoteFullDb, userId: Int, date: String
    ): List<Task> {
        return room.taskDao().getAllTaskByUserIdAndDate(userId, date)
    }

    private fun replaceFragment(fragment: Fragment) {
        requireActivity().supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentContainerView, fragment)
            commit()
        }
    }

}
