package com.example.notefullproyect.task.data

import androidx.room.*
import com.example.notefullproyect.task.model.Task

/**
 * Functions that allow communication with the database for Task entity.
 * @author Jessica Castillo
 */
@Dao
interface TaskDao {

    @Insert
    suspend fun insertTask(task: Task)

    @Query("SELECT * FROM Task WHERE userId = :userId and date = :date ORDER BY hour ASC, priorityId ASC")
    fun getAllTaskByUserIdAndDate(userId: Int, date: String): List<Task>

    @Delete
    suspend fun deleteTask(task: Task)

    @Query("DELETE FROM Task WHERE id = :id")
    suspend fun deleteTaskById(id: Int)

    @Query("SELECT * FROM Task WHERE id IN (:id)")
    fun getTaskById(id: Int): Task

    @Update
    suspend fun updateTask(task: Task)

}