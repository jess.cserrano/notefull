package com.example.notefullproyect.task.presentation

import android.annotation.SuppressLint
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.notefullproyect.R
import com.example.notefullproyect.database.NoteFullDb
import com.example.notefullproyect.databinding.ItemPrincipalTaskBinding
import com.example.notefullproyect.task.model.Task
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

/**
 * From the item of the cardView adapt the content of the task to each cardView of the recyclerView.
 * @author Jessica Castillo
 */
class TaskAdapter(private val listTask: List<Task>) :
    RecyclerView.Adapter<TaskAdapter.ViewHolder>() {

    private lateinit var mListener: OnItemClickListener
    private lateinit var room: NoteFullDb

    /**
     * Interface that extends of OnClickListener
     * @author Jessica Castillo
     */
    interface OnItemClickListener : View.OnClickListener {
        /**
         * Method without implementer
         * @param position
         * @author Jessica Castillo
         */
        fun onItemClick(position: Int)
    }

    /**
     *Sets a value for the local variable mListener.
     * @param listener
     * @author Jessica Castillo
     */
    fun setOnItemClickListener(listener: OnItemClickListener) {
        mListener = listener
    }


    class ViewHolder(
        private val binding: ItemPrincipalTaskBinding,
        listener: OnItemClickListener,
        room: NoteFullDb
    ) : RecyclerView.ViewHolder(binding.root) {

        var name = binding.tvNameCardTask
        var ivToDo = binding.ivToDo
        var ivDoing = binding.ivDoing
        var ivDone = binding.ivDone
        var pomo = binding.tvSetPomodoroCard
        var roomDB: NoteFullDb = room


        @SuppressLint("SetTextI18n")
        fun bind(item: Task) {

            name.text = item.title
            binding.etMultiTask.text = item.description
            binding.tvHourCardTask.text = item.hour
            pomo.text = item.quantityTime.toString()

            val priority = binding.tvTaskPriorityStatus
            when (item.priorityId) {
                1 -> priority.text = "Alta"
                2 -> priority.text = "Media"
                3 -> priority.text = "Baja"
            }
            when (item.progressId) {
                1 -> {
                    ivToDo.setImageResource(R.drawable.ic_circle_todo_check)
                    ivDoing.setImageResource(R.drawable.ic_circle_doing)
                    ivDone.setImageResource(R.drawable.ic_circle_done)
                    binding.cardviewTask.setCardBackgroundColor(Color.parseColor("#26EE9F93"))
                }
                2 -> {
                    ivDoing.setImageResource(R.drawable.ic_circle_doing_check)
                    ivToDo.setImageResource(R.drawable.ic_circle_todo)
                    ivDone.setImageResource(R.drawable.ic_circle_done)
                    binding.cardviewTask.setCardBackgroundColor(Color.parseColor("#26253DB5"))
                }
                3 -> {
                    ivDone.setImageResource(R.drawable.ic_circle_done_check)
                    ivToDo.setImageResource(R.drawable.ic_circle_todo)
                    ivDoing.setImageResource(R.drawable.ic_circle_doing)
                    binding.cardviewTask.setCardBackgroundColor(Color.parseColor("#66ACCCD3"))
                }
            }

            ivToDo.setOnClickListener {

                ivToDo.setImageResource(R.drawable.ic_circle_todo_check)
                ivDoing.setImageResource(R.drawable.ic_circle_doing)
                ivDone.setImageResource(R.drawable.ic_circle_done)
                binding.cardviewTask.setCardBackgroundColor(Color.parseColor("#26EE9F93"))
                updateProgressTask(roomDB, item.id, 1)
            }

            ivDoing.setOnClickListener {
                ivDoing.setImageResource(R.drawable.ic_circle_doing_check)
                ivToDo.setImageResource(R.drawable.ic_circle_todo)
                ivDone.setImageResource(R.drawable.ic_circle_done)
                binding.cardviewTask.setCardBackgroundColor(Color.parseColor("#26253DB5"))
                updateProgressTask(roomDB, item.id, 2)
            }

            ivDone.setOnClickListener {
                ivDone.setImageResource(R.drawable.ic_circle_done_check)
                ivToDo.setImageResource(R.drawable.ic_circle_todo)
                ivDoing.setImageResource(R.drawable.ic_circle_doing)
                binding.cardviewTask.setCardBackgroundColor(Color.parseColor("#66ACCCD3"))
                updateProgressTask(roomDB, item.id, 3)
            }

        }

        init {
            itemView.setOnClickListener {
                listener.onItemClick(absoluteAdapterPosition)
            }
        }

        private fun updateProgressTask(room: NoteFullDb, taskId: Int, progressId: Int) {
            val task = room.taskDao().getTaskById(taskId)
            task.progressId = progressId

            GlobalScope.launch {
                room.taskDao().updateTask(task)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)

        room = NoteFullDb.getDatabase(parent.context)

        val listItemBinding = ItemPrincipalTaskBinding.inflate(inflater, parent, false)
        return ViewHolder(listItemBinding, mListener, room)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.bind(listTask[position])
    }

    override fun getItemCount(): Int {
        return listTask.size
    }

}
